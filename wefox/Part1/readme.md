### Notes:
* Number of Rooms could be '0'?
* Suggested flights are not related to users� current location
* Check-in and check-out could be in the past
* Can't search for a city name by typing a new of a country. 'Germany' returns 0 results- should show German cities?
* Searching for flights tab does not have a checkbox for hotel too. a feature that could be needed


<br>

### Finidngs:
#### Exploring flows were users could book a hotel-

There's 3 sceanrios which the user could book a hotel:

* 1) Booking a hotel directly, while choosing a specific city, date, and number of people.

* 2) Choosing a flight, the user should also pick a hotel for his stay, a feature that does not fully implemented as for now.

* 3) Choosing a tour, the package contains a hotel stay if it's more than 1 day trip, but the hotel information is not shown, neither the opportunity to pick one.
<br>
###### nevertheless, there's few issues found:

* Rooms number could be 0. it should start from 1
* Although they can't pick date in the past, the users could navigate to the previous months to pick the check-in date. Also, when choosing a check-out date, they can navigate to dates before check-in date.


#### How To choose which flow is more critical?

* There's more than one way to choose which flow is more important to test first. if the sales data shows that 
	most users book more tours? Flights? or hotels? 
	then that flow would be priorities first. but naturally, since it's a hotel finding website and assuming that 
	users use it for this cause, the most critical feature would be making sure that the functionality of booking 
	a hotel, directly, works as expected.

#### Risks?

* Risks would be the senstive information as credit card and details of users.
