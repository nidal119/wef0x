##### conclusion:

The tests I have written are a mostly a unit testing with little integration aspect
On one hand, I wrote tests that are at some cases connected; 
like verifying if a pet existing then deleteing it and making sure it was indeed deleted.
and on the other hand, this basic flow could not verify completly that a series of flows was implemented successfully.
<br>

##### what whould be the prefect strategy for those tests?

In a perfect world. those tests should be an integration tests. 
it's not only important to verify that one pet was added. but also it's importnant to make sure that this exact pet
was added to the DB, also that we can do more actions to it -like updating its attributes and deleting it completly.
Unit testing takes much more time to implement, since we need to verify each step as a whole.
integration tests is more efficient since we can test more than more flow in one test

