#### -How to Run the test?
* Option 1) Navigate to src/test/java/wefox/Runner/RunnerHelper and click 'run'
* Option 2) First, click on the project -> Maven -> Update project Then, click on the project -> Run as -> TestNG Test

##### -Which tools did I use?
* Java, Cucumber, RestAssured, Eclipse 

##### -Diagram:
![Screenshot](test-output/automation_diagram.png)
<br>
###### - Part1 is attached to the project
###### - Part3 is attached to the project
