package wefox.actualFunctions;

import static io.restassured.RestAssured.given;

import com.google.gson.JsonObject;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class BaseRequests {
	private Response response;

	public Response get(String baseUri, String path) {
		response = given().baseUri(baseUri).contentType(ContentType.JSON).get(path);
		return response;
	}

	public Response post(JsonObject body, String bASE_PETSTORE, String string) {
		response = given().baseUri(bASE_PETSTORE).contentType(ContentType.JSON).body(body).post("");
		return response;
	}

	public Response put(JsonObject body, String bASE_PETSTORE, String string) {
		response = given().baseUri(bASE_PETSTORE).contentType(ContentType.JSON).body(body).put("");
		return response;
	}

	public Response delete(String bASE_PETSTORE, String path) {
		response = given().baseUri(bASE_PETSTORE).contentType(ContentType.JSON).delete(path);
		return response;
	}

}
