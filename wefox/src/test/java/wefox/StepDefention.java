package wefox;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import wefox.stepFunction.PetStore_StepFunctions;

public class StepDefention {
	private PetStore_StepFunctions petStore_StepFunctions = new PetStore_StepFunctions();

	@Given("Verify a pet you are trying to add does not already exist")
	public void verify_a_pet_you_are_trying_to_add_does_not_already_exist() {
		// Write code here that turns the phrase above into concrete actions
		this.petStore_StepFunctions.verifyPetDoesNTExists();
	}

	@When("Adding new pet to the store named {string} and status {string}")
	public void adding_new_pet_to_the_store(String name, String status) {
		// Write code here that turns the phrase above into concrete actions
		this.petStore_StepFunctions.AddingNewPet(name, status);
	}

	@When("Verify new pet added to the store")
	public void verify_new_pet_added_to_the_store() {
		// Write code here that turns the phrase above into concrete actions
		this.petStore_StepFunctions.verifyPetAdded();
	}

	@Given("get pets by status {string}")
	public void get_pets_by_status(String status) {
		// Write code here that turns the phrase above into concrete actions
		this.petStore_StepFunctions.getPetByStatus(status);
	}

	@When("update the name of the lowest id pet found")
	public void update_first_pet_name_on_the_list() {
		// Write code here that turns the phrase above into concrete actions
		this.petStore_StepFunctions.updateNameOfsmallestIdfound();
	}

	@When("Verify pets name was updated")
	public void verify_pet_s_name_was_updated() {
		// Write code here that turns the phrase above into concrete actions
		this.petStore_StepFunctions.verifyNameWasUpdated();
	}

	@When("delete random bet from store by status")
	public void delete_the_pet_from_store() {
		// Write code here that turns the phrase above into concrete actions
		this.petStore_StepFunctions.deleteRandomPetByStatus();
	}

	@When("Verify pet was deleted from store")
	public void verify_pet_was_deleted_from_store() {
		// Write code here that turns the phrase above into concrete actions
		this.petStore_StepFunctions.verifyPetDeleted();
	}

}
