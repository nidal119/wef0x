package wefox.stepFunction;

import static org.testng.Assert.assertTrue;
import java.util.List;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import io.restassured.http.ContentType;
import wefox.Runner.World;
import wefox.actualFunctions.BaseRequests;

public class PetStore_StepFunctions {
	private World world = new World();
	private BaseRequests baseRequest = new BaseRequests();
	private final String BASE_PETSTORE = "https://petstore3.swagger.io/api/v3/pet/";
	private String searchUsing_ID = "ID/";
	private String searchUsingStatus = "findByStatus?status=xxx";

	public void verifyPetDoesNTExists() {
		world.setPet();
		String temp;
		temp = searchUsing_ID.replaceAll("ID", String.valueOf(this.world.getPet().getId()));
		this.world.setCurrentResponse(this.baseRequest.get(BASE_PETSTORE, temp));
		this.world.getCurrentResponse().then().assertThat().statusCode(404);
		assertTrue(this.world.getCurrentResponse().asPrettyString().contains("Pet not found"), "Pet already exist!");
	}

	public void AddingNewPet(String name, String status) {
		System.out.println("Adding new pet with ID: " + this.world.getPet().getId() + " and status: " + status);
		JsonObject body = petJsonBuilder(this.world.getPet().getId(), name, status);
		this.world.setCurrentResponse(this.baseRequest.post(body, BASE_PETSTORE, ""));
		this.world.getCurrentResponse().then().assertThat().statusCode(200).and().assertThat()
				.contentType(ContentType.JSON);
	}

	public void verifyPetAdded() {
		String temp;
		temp = searchUsing_ID.replaceAll("ID", String.valueOf(this.world.getPet().getId()));
		this.world.setCurrentResponse(this.baseRequest.get(BASE_PETSTORE, temp));
		assertTrue(this.world.getCurrentResponse().getStatusCode() == 200, "Pet was NOT added successfully!");
		this.world.getCurrentResponse().then().assertThat().statusCode(200).and().assertThat()
				.contentType(ContentType.JSON);
		assertTrue(this.world.getCurrentResponse().then().extract().path("id").toString()
				.equals(String.valueOf(this.world.getPet().getId())), "reponse's petID is not equal to created one!");
		System.out.println("Pet was added successfully!");
	}

	public JsonObject petJsonBuilder(Integer id, String name, String status) {
		JsonObject jsonBody = new JsonObject();
		jsonBody.addProperty("id", id);
		jsonBody.addProperty("name", name);
		JsonObject category = new JsonObject();
		category.addProperty("id", 1);
		category.addProperty("name", "Dogs");
		jsonBody.add("category", category);
		JsonArray photoUrls = new JsonArray();
		photoUrls.add("string");
		jsonBody.add("photoUrls", photoUrls);
		JsonArray tags = new JsonArray();
		tags.add(category);
		jsonBody.add("tags", tags);
		jsonBody.addProperty("status", status);
		return jsonBody;
	}

	public void getPetByStatus(String status) {
		System.out.println("Getting Pet by status: " + status);
		String temp;
		temp = searchUsingStatus.replaceAll("xxx", status);
		this.world.setCurrentResponse(this.baseRequest.get(BASE_PETSTORE, temp));
		this.world.getCurrentResponse().then().assertThat().statusCode(200);
	}

	public void updateNameOfsmallestIdfound() {
		List<Integer> idList = this.world.getCurrentResponse().then().extract().jsonPath().getList("id");
		Integer smallestID = Integer.valueOf(idList.get(0));
		this.world.setsmallestID(smallestID);
		int randomNumber = (int) Math.floor(Math.random() * (99000)) + 101;
		String temp = "newName" + randomNumber;
		this.world.setNewName(temp);
		JsonObject body = petJsonBuilder(smallestID, temp, "available");
		this.world.setCurrentResponse(this.baseRequest.put(body, BASE_PETSTORE, ""));
	}

	public void verifyNameWasUpdated() {
		System.out.println("Verify name was updated!");
		assertTrue(this.world.getCurrentResponse().getStatusCode() == 200, "Name was NOT updated!");
		assertTrue(
				this.world.getCurrentResponse().then().extract().path("name").toString()
						.equals(this.world.getNewName()),
				"Name was not changed! expected: " + this.world.getNewName() + " -Found: "
						+ this.world.getCurrentResponse().then().extract().path("name").toString());
		System.out.println("Name was updated successfully");
	}

	public void deleteRandomPetByStatus() {
		System.out.println("Deleting Random pet by status!");
		List<Integer> idList = this.world.getCurrentResponse().then().extract().jsonPath().getList("id");
		Integer smallestID = Integer.valueOf(idList.get(0));
		this.world.setsmallestID(smallestID);
		String temp = searchUsing_ID.replaceAll("ID", String.valueOf(smallestID));
		this.world.setCurrentResponse(this.baseRequest.delete(BASE_PETSTORE, temp));
		this.world.getCurrentResponse().then().assertThat().statusCode(200);
		assertTrue(this.world.getCurrentResponse().asPrettyString().contains("Pet deleted"), "Pet was not deleted!");
	}

	public void verifyPetDeleted() {
		String temp = searchUsing_ID.replaceAll("ID", String.valueOf(this.world.getSmallestID()));
		this.world.setCurrentResponse(this.baseRequest.get(BASE_PETSTORE, temp));
		assertTrue(this.world.getCurrentResponse().getStatusCode() == 404, "Pet was NOT deleted successfully!");
		assertTrue(this.world.getCurrentResponse().asPrettyString().contains("Pet not found"), "Pet already exist!");
		System.out.println("Pet was deleted successfully");
	}
}
