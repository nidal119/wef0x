package wefox.Runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

public class RunnerHelper {

@CucumberOptions(
        features="src/test/resources/features",
        tags = "@petstore",//create,update,delete,|petstore
        glue="wefox",
        plugin = {"json:target/cucumber-report/cucumber.json"}
        )

public class TestNGRunner extends AbstractTestNGCucumberTests {
}
}