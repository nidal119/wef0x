package wefox.Runner;

import com.microsoft.playwright.Page;
import io.restassured.response.Response;
import wefoxFiles.Pet;

public class World {
	private Page page;
	private Pet pet;
	private Response currentResponse;
	private String newName;
	private Integer smallestID;

	public void setPage(Page page) {
		this.page = page;
	}

	public Page getPage() {
		return page;
	}

	public Pet getPet() {
		return pet;
	}

	public Response getCurrentResponse() {
		return currentResponse;
	}

	public void setCurrentResponse(Response currentResponse) {
		this.currentResponse = currentResponse;
	}

	public Integer setPet() {
		pet = new Pet();
		Integer petID = (int) Math.floor(Math.random() * (99000)) + 90;
		pet.setId(petID);
		return petID;
	}

	public void setsmallestID(Integer smallestID2) {
		this.smallestID = smallestID2;
	}

	public Integer getSmallestID() {
		return smallestID;
	}

	public void setNewName(String newName) {
		this.newName = newName;
	}

	public String getNewName() {
		return newName;
	}
}