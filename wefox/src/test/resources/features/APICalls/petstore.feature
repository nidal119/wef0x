@petstore
Feature: Create, Update, Delete petstore

@create
  Scenario: Add new pet to the store
    Given Verify a pet you are trying to add does not already exist
    When Adding new pet to the store named 'dogy' and status 'available'
    And Verify new pet added to the store
    
@update
  Scenario: update pet deatils in the store
    Given get pets by status 'available'
    When update the name of the lowest id pet found
    And Verify pets name was updated
    
@delete
  Scenario: delete pet
    Given get pets by status 'sold'
    When delete random bet from store by status
    And Verify pet was deleted from store
    