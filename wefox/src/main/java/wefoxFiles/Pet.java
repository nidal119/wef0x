package wefoxFiles;

public class Pet {
	Integer id;
	String name;
	String tags;
	String status;
	String photoUrls;

	public Integer getId() {
		return id;
	}

	public void setId(Integer petID) {
		this.id = petID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setPhotoUrls(String photoUrls) {
		this.photoUrls = photoUrls;
	}

	public String getPhotoUrls() {
		return photoUrls;
	}

}
